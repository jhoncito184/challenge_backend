import { Repository } from 'typeorm';
import { Tag } from './entities/tag.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class TagsService {
  constructor(
    @InjectRepository(Tag)
    private tagsRepository: Repository<Tag>,
  ) {}

  async find(where) {
    return this.tagsRepository.findOne({ where: where });
  }

  async findAll() {
    return this.tagsRepository.find();
  }

  async findOne(id: number) {
    return this.tagsRepository.findOne({ where: { id: id } });
  }

  async save(data) {
    return this.tagsRepository.save(data);
  }
}
