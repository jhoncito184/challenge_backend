import { Module } from '@nestjs/common';
import { Tag } from './entities/tag.entity';
import { TagsService } from './tags.service';
import { TypeOrmModule } from '@nestjs/typeorm';
// import { TagsController } from './tags.controller';

@Module({
  imports: [TypeOrmModule.forFeature([Tag])],
  // controllers: [TagsController],
  providers: [TagsService],
})
export class TagsModule {}
