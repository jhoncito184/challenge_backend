import { Module } from '@nestjs/common';
import { Post } from './entities/post.entity';
import { PostsService } from './posts.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PostsController } from './posts.controller';
import { GetPostServiceService } from './get-post-service.service';
import { HttpModule } from '@nestjs/axios';
import { TagsService } from '../tags/tags.service';
import { Tag } from './entities/tag.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Post, Tag]), HttpModule],
  controllers: [PostsController],
  providers: [PostsService, TagsService, GetPostServiceService],
})
export class PostsModule {}
