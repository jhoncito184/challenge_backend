export interface PostsDTO {
  id: number;
  title: string;
  content: string;
  author: string;
  url: string;
}
