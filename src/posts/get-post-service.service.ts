import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { Post } from '../posts/entities/post.entity';
import { TagsService } from '../tags/tags.service';
import { PostsService } from '../posts/posts.service';
import { Tag } from './entities/tag.entity';

@Injectable()
export class GetPostServiceService {
  constructor(
    private httpService: HttpService,
    private readonly tagsService: TagsService,
    private readonly postsService: PostsService,
  ) {}

  @Cron(CronExpression.EVERY_30_SECONDS)
  async handleCron() {
    const API_POSTS =
      'https://hn.algolia.com/api/v1/search_by_date?query=nodejs';

    this.httpService.get(API_POSTS).subscribe(async (response) => {
      const data = response.data;

      console.log('start cron');

      if (data.hits && data.hits.length > 0) {
        for (const post of data.hits) {
          const postExists = await this.postsService.find({
            objectID: post.objectID,
          });

          if (postExists) {
            continue;
          }

          const newTags = [];
          const tags = post._tags;

          if (tags) {
            for (const tag of tags) {
              const tagExists = await this.tagsService.find({ name: tag });

              if (!tagExists) {
                const newTag = new Tag();
                newTag.name = tag;
                await this.tagsService.save(newTag);

                newTags.push(newTag);

                continue;
              }

              newTags.push(tagExists);
            }
          }

          console.log(newTags);

          const newPost = new Post();
          newPost.title = post.title;
          newPost.url = post.url;
          newPost.author = post.author;
          newPost.createdAt = post.created_at;
          newPost.content = post.comment_text;
          newPost.objectID = post.objectID;
          newPost.tags = newTags;

          this.postsService.save(newPost);
        }
      }
    });

    console.log('end cron');
  }
}
