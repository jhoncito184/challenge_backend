import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Post } from './entities/post.entity';
import { PostsDTO } from './posts.dto';

@Injectable()
export class PostsService {
  // constructor(
  //   @InjectRepository(Post)
  //   private postsRepository: Repository<Post>,
  // ) {}

  // findAll(): Promise<Post[]> {
  //   return this.postsRepository.find();
  // }

  // findOne(id: string): Promise<Post> {
  //   return this.postsRepository.findOne(id);
  // }

  // async remove(id: string): Promise<void> {
  //   await this.postsRepository.delete(id);
  // }
  readonly monthsRelations = {
    january: '01',
    february: '02',
    march: '03',
    april: '04',
    may: '05',
    june: '06',
    july: '07',
    august: '08',
    september: '09',
    october: '10',
    november: '11',
    december: '12',
  };

  constructor(
    @InjectRepository(Post)
    private postsRepository: Repository<Post>,
  ) {}

  async findAll(
    author?: string,
    title?: string,
    month?: string,
    tags?: Array<string>,
  ) {
    const postQueryBuilder = this.postsRepository.createQueryBuilder('posts');

    if (author) {
      postQueryBuilder.where('author = :author', { author: author });
    }

    if (title) {
      postQueryBuilder.andWhere('title = :title', { title: title });
    }

    if (month) {
      postQueryBuilder.andWhere('EXTRACT(MONTH FROM created_at) = :month', {
        month: this.monthsRelations[month],
      });
    }

    if (tags && tags.length > 0) {
      postQueryBuilder
        .innerJoinAndSelect('posts.tags', 'tags')
        .orWhere('tags.name IN (:...tags)', { tags: tags });
    }

    return postQueryBuilder.getMany();
  }

  async findByEmail(email: string): Promise<PostsDTO> {
    return this.postsRepository.findOne({
      where: {
        email: email,
      },
    });
  }

  async findOne(id: number) {
    return this.postsRepository.findOne({ where: { id: id } });
  }

  async find(where) {
    return this.postsRepository.findOne({ where: where });
  }

  async save(data: Post) {
    return this.postsRepository.save(data);
  }

  async remove(id: number) {
    await this.postsRepository.delete({ id });
    return { deleted: true };
  }
}
