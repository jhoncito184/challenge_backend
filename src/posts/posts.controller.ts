import { PostsService } from './posts.service';
import { Controller, Get, Param, Delete, Body, Query } from '@nestjs/common';

@Controller('posts')
export class PostsController {
  constructor(private readonly postsService: PostsService) {}

  @Get()
  findAll(
    @Body('author') author?: string,
    @Body('title') title?: string,
    @Query('month') month?: string,
    @Query('_tags') tags?: Array<string>,
  ) {
    return this.postsService.findAll(author, title, month, tags);
  }

  @Get(':id')
  findOne(@Param('id') id: number) {
    return this.postsService.findOne(id);
  }

  @Delete(':id')
  remove(@Param('id') id: number) {
    return this.postsService.remove(id);
  }
}
